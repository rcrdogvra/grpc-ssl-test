﻿using System;
using System.IO;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Logging;

namespace TestgRPC
{
  class Program
  {
    static void Main()
    {
      Environment.SetEnvironmentVariable("GRPC_VERBOSITY", "DEBUG");
      Environment.SetEnvironmentVariable("GRPC_TRACE", "connectivity_state");
      GrpcEnvironment.SetLogger(new ConsoleLogger());
      var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location.Substring(0, System.Reflection.Assembly.GetExecutingAssembly().Location.IndexOf("bin")));
;
      Server grpcServer = new Server();
      try
      {
        string serverCert = File.ReadAllText(path + "/../server.crt");
        string serverKey = File.ReadAllText(path + "/../server.key");
        var keypair = new KeyCertificatePair(serverCert, serverKey);
        SslServerCredentials credentials = new SslServerCredentials(new[] { keypair });
        grpcServer.Ports.Add(new ServerPort("0.0.0.0", 8245, credentials));

        Task.Run(()=>
        {
          grpcServer.Start();
        });
        Console.WriteLine("Server running...");
        Console.ReadLine();
      }
      finally
      {
        grpcServer.ShutdownAsync().Wait();
      }
    }
  }
}
