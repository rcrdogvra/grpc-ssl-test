﻿using System;
using System.IO;
using Grpc.Core;
using Grpc.Core.Logging;

namespace TestClientGRPC
{
  class Program
  {
    static void Main()
    {
      Environment.SetEnvironmentVariable("GRPC_VERBOSITY", "DEBUG");
      Environment.SetEnvironmentVariable("GRPC_TRACE", "connectivity_state");
      var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location.Substring(0, System.Reflection.Assembly.GetExecutingAssembly().Location.IndexOf("bin")));
      path = path + "/../ca.crt";
      Environment.SetEnvironmentVariable("GRPC_DEFAULT_SSL_ROOTS_FILE_PATH", path);
      GrpcEnvironment.SetLogger(new ConsoleLogger());

      SslCredentials sslCredentials = new SslCredentials();
      var channel = new Channel("localhost:8245", sslCredentials);
      Console.WriteLine("Connecting");
      channel.ConnectAsync().Wait();
    }
  }
}
