# Run the Server

```bash
dotnet run --project server/server.csproj
```


# Run the Client
```
dotnet run --project client/client.csproj
```